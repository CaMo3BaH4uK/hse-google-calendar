from os import environ
from HSE_Google_Calendar import createApp
import sys
sys.path.append('/usr/local/lib/python3.6/dist-packages')

app = createApp()
if environ.get('INIT_BASE', 0) == '1':
    from HSE_Google_Calendar import db, createApp
    db.create_all(app=createApp())
    sys.exit(0)
    

if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(environ.get('SERVER_PORT', '5000'))
    except ValueError:
        PORT = 5000
    app.run(HOST, PORT, ssl_context="adhoc")