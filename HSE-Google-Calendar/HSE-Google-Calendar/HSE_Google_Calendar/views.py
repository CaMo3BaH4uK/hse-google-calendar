from flask import render_template, Blueprint, redirect, url_for
from flask_login import login_required, current_user
from HSE_Google_Calendar import db
from .ruz import checktoken


main = Blueprint('main', __name__)

@main.route('/')
def index():
    if checktoken(current_user):
        return redirect(url_for("auth.logout"))
    
    currentPageTitle = "Index"
    return render_template('index.html', currentPageTitle=currentPageTitle)

@main.route('/privacy')
def privacy():
    return redirect(url_for('static', filename="privacy.pdf"))
