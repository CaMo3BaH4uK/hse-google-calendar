from HSE_Google_Calendar import db
from flask_login import UserMixin


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    gid = db.Column(db.String(100))
    token = db.Column(db.String(4000))
    refresh_token = db.Column(db.String(1000))
    name = db.Column(db.String(1000))
    ruzid = db.Column(db.Integer)
