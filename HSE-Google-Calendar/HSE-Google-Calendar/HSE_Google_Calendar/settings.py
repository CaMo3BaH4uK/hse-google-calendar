from datetime import datetime
from flask import render_template, Blueprint, redirect, url_for, request, flash
from flask_login import login_user, logout_user, login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from HSE_Google_Calendar import db 
from .models import User
from .ruz import ruz, checktoken
settings = Blueprint('settings', __name__)



@settings.route('/settings')
@login_required
def index():
    if checktoken(current_user):
        return redirect(url_for("auth.login"))
    currentPageTitle = "Settings"
    return render_template('settings.html', currentPageTitle=currentPageTitle, name=current_user.name, ruzid=current_user.ruzid)


@settings.route('/settings/namechange', methods=['POST'])
@login_required
def namechange():
    if checktoken(current_user):
        return redirect(url_for("auth.login"))
    name = str(request.form.get('name'))

    if not len(name):
        flash('Enter correct name')
        return redirect(url_for('settings.index'))

    ruz(current_user).updateruzname(name)

    return redirect(url_for('settings.index'))

@settings.route('/settings/getschedule')
@login_required
def getschedule():
    if checktoken(current_user):
        return redirect(url_for("auth.login"))
    ruz(current_user).getschedule()
    return redirect(url_for('settings.index'))

@settings.route('/settings/clearschedule')
@login_required
def clearschedule():
    if checktoken(current_user):
        return redirect(url_for("auth.login"))
    ruz(current_user).clearschedule()
    return redirect(url_for('settings.index'))

