from .google import GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET
from google.oauth2.credentials import Credentials
from gcsa.event import Event
from gcsa.google_calendar import GoogleCalendar
from datetime import datetime, timedelta

class gcal(object):
    def __init__(self, user):
        self.token = Credentials(
            token=user.token,
            refresh_token=user.refresh_token,
            client_id=GOOGLE_CLIENT_ID,
            client_secret=GOOGLE_CLIENT_SECRET,
            scopes=['https://www.googleapis.com/auth/calendar'],
            token_uri='https://oauth2.googleapis.com/token'
        )
        self.gc = GoogleCalendar(credentials=self.token)
        
    def connect(self):
        try:
            events = self.gc.get_events(time_min=datetime.now(),time_max=datetime.now(), query='hse@ethernet.su')
            for event in events:
                pass
        except:
            return False
        return True

    def addevent(self, lesson_name, lesson_location, lesson_start, lesson_end, lesson_description):
        event = Event(
            lesson_name,
            start=lesson_start,
            end=lesson_end,
            description="{}<br>{}".format(lesson_description, "hse@ethernet.su"),
            location=lesson_location,
            minutes_before_popup_reminder=20
        )
        self.gc.add_event(event)

    def clearevents(self):
        time_start = datetime.now() - timedelta(days=3*365)
        time_end = datetime.now() + timedelta(days=3*365)
        events = self.gc.get_events(time_min=time_start,time_max=time_end, query='hse@ethernet.su')
        for event in events:
            self.gc.delete_event(event)


