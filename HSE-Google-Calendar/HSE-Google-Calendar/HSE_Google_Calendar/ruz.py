from HSE_Google_Calendar import db 
from .gcal import gcal
from .models import User
import urllib.request, urllib.parse, json
from datetime import datetime, date, timedelta
import calendar
import sys

class ruz(object):
    def __init__(self, user):
        self.user = user
        self.gcal = gcal(self.user)
        self.gcal.connect()

    def updateruzname(self, name):
        self.user.ruzid = -1
        with urllib.request.urlopen("https://ruz.hse.ru/api/search?term={}&type=student".format(urllib.parse.quote(name.encode('utf8')))) as url:
            data = json.loads(url.read().decode())
            try:
                self.user.ruzid = int(data[0]['id'])
                self.user.name = data[0]['label']
            except:
                pass
        if self.user.ruzid != -1:
            import threading
            thread = threading.Thread(target=self.localupdateruz, args=(self.user.ruzid,))
            thread.daemon = True
            thread.start()
        db.session.commit()

    def localupdateruz(self, ruzid):
        print("Local Update started for ruzid {}".format(ruzid), file = sys.stderr ) 
        try:
            self.user = User
            self.user.ruzid = ruzid
            self.clearschedule()
            self.getschedule()
        except Exception as e:
            print("Local Update failed", file = sys.stderr ) 
            print(e, file = sys.stderr ) 
        print("Local Update finished", file = sys.stderr ) 

    def updateruz(self):
        print("Update for user {} started".format(self.user.id), file = sys.stderr )
        try:
            if self.user.ruzid == -1:
                with urllib.request.urlopen("https://ruz.hse.ru/api/search?term={}&type=student".format(urllib.parse.quote(self.user.name.strip().encode('utf8')))) as url:
                    data = json.loads(url.read().decode())
                    try:
                        self.user.ruzid = int(data[0]['id'])
                        self.user.name = data[0]['label']
                    except:
                        pass
                db.session.commit()
                print("Fixed ruzid to {}".format(self.user.ruzid), file = sys.stderr )
            self.clearschedule()
            self.getschedule()
        except Exception as e:
            print("Update for user {} failed".format(self.user.id), file = sys.stderr ) 
            print(e, file = sys.stderr )
        print("Update for user {} finished".format(self.user.id), file = sys.stderr )

    def getschedule(self):
        if self.user.ruzid != -1:
            date_start = date.today() - timedelta(days=date.today().weekday())
            days_in_month = calendar.monthrange(datetime.now().year, datetime.now().month)[1]
            date_end = datetime.now() + timedelta(days=days_in_month)
            with urllib.request.urlopen("https://ruz.hse.ru/api/schedule/student/{}?start={}&finish={}&lng=1".format(self.user.ruzid, date_start.strftime("%Y.%m.%d"), date_end.strftime("%Y.%m.%d"))) as url:
                lessons = json.loads(url.read().decode())
                for lesson in lessons:
                    lesson_name = '{} {}'.format(lesson['kindOfWork'],lesson['discipline'])
                    lesson_location = '{} {}'.format(lesson['building'],lesson['auditorium'])
                    lesson_start = datetime.strptime('{} {}'.format(lesson['date'],lesson['beginLesson']), '%Y.%m.%d %H:%M')
                    lesson_end = datetime.strptime('{} {}'.format(lesson['date'],lesson['endLesson']), '%Y.%m.%d %H:%M')
                    lesson_description = '{}<br>{}<br>{}<br>{}<br>{}'.format(lesson['lecturer'],lesson['url1'],lesson['url1_description'],lesson['url2'],lesson['url2_description']).replace('None', '')
                    self.gcal.addevent(lesson_name, lesson_location, lesson_start, lesson_end, lesson_description)
        else:
            print("Ruzid is bad: {}".format(self.user.ruzid), file = sys.stderr )


    def clearschedule(self):
        self.gcal.clearevents()


def checktoken(current_user):
    if current_user.is_authenticated:
        return not ruz(current_user).gcal.connect()
            