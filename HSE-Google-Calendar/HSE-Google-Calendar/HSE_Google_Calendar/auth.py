from datetime import datetime
from flask import render_template, Blueprint, redirect, url_for, request, flash
from flask_login import login_user, logout_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from HSE_Google_Calendar import db
from .models import User
from oauthlib.oauth2 import WebApplicationClient
from .google import GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, GOOGLE_DISCOVERY_URL, getgoogleprovidercfg
from .ruz import ruz, checktoken

import os
import requests
import json


auth = Blueprint('auth', __name__)


client = WebApplicationClient(GOOGLE_CLIENT_ID)


@auth.route('/login')
def login():
    google_provider_cfg = getgoogleprovidercfg()
    authorization_endpoint = google_provider_cfg["authorization_endpoint"]

    request_uri = client.prepare_request_uri(
        authorization_endpoint,
        redirect_uri=request.base_url + "/callback",
        scope=["openid", "email", "profile", "https://www.googleapis.com/auth/calendar"],
        access_type='offline',
        prompt='consent'
    )
    return redirect(request_uri)

@auth.route("/login/callback")
def callback():
    code = request.args.get("code")

    google_provider_cfg = getgoogleprovidercfg()
    token_endpoint = google_provider_cfg["token_endpoint"]

    token_url, headers, body = client.prepare_token_request(
        token_endpoint,
        authorization_response=request.url,
        redirect_url=request.base_url,
        code=code,
    )
    token_response = requests.post(
        token_url,
        headers=headers,
        data=body,
        auth=(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET),
    )

    token_response_json = token_response.json()
    client.parse_request_body_response(json.dumps(token_response_json))

    userinfo_endpoint = google_provider_cfg["userinfo_endpoint"]
    uri, headers, body = client.add_token(userinfo_endpoint)
    userinfo_response = requests.get(uri, headers=headers, data=body)

    if userinfo_response.json().get("email_verified"):
        unique_id = userinfo_response.json()["sub"]
        users_email = userinfo_response.json()["email"]
        picture = userinfo_response.json()["picture"]
        users_name = userinfo_response.json()["given_name"]
    else:
        return "User email not available or not verified by Google.", 400

    user = User.query.filter_by(email=users_email).first()

    if not user:
        user = User(email=users_email, gid=unique_id, name=users_name, ruzid = -1, token=token_response_json['access_token'], refresh_token=token_response_json['refresh_token'])
        db.session.add(user)
    else:
        user.email=users_email
        user.gid=unique_id
        user.token=token_response_json['access_token']
        user.refresh_token=token_response_json['refresh_token']

    db.session.commit()

    login_user(user)
    
    return redirect(url_for("main.index"))

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))

@auth.route('/api/updateschedule/<key>')
def updateschedule(key):
    if key == os.environ.get("SECRET_KEY"):
        import sys
        print("Update started", file = sys.stderr ) 
        from .models import User
        users = User.query.all()
        for user in users:
            ruz(user).updateruz()
        print("Update finished", file = sys.stderr ) 
    return "Update finished"
    